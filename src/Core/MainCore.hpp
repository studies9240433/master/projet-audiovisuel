//
// Created by pierre on 01/01/19.
//

#ifndef PROJECT_MAINCORE_HPP
#define PROJECT_MAINCORE_HPP

#include <Core/Video/MainVideo.hpp>
#include <Core/Sound/Sound.hpp>
#include "Macros.hpp"
#include "Video/LucasKanade.hpp"

class MainCore {
public:

    MainCore();

    void setVideo(std::string name);
    void useWebcam();
    void setFrame(int f);
    bool displayFrame();
    void displayOpticalFlow();
    void playVideo();

    void setMethodLK(int i);
    void toggleDebugMode();
    void toggleShepard(bool val);

    /* DEBUG */
    void debug();

    MainVideo mainVideo;
    Sound sound;

private:

    cv::VideoCapture video;
    int frame = 1,totalFrames;

    Mat Vx,Vy;

    int methodLK = 0;
    bool debugMode = true;
    bool shepardSteps = false;


};


#endif //PROJECT_MAINCORE_HPP
