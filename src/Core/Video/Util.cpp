//
// Created by pierre on 03/02/19.
//

#include "Util.hpp"

void filter_low(Points &p1, Points &p2, int val) {
    Points p1b;
    Points p2b;
    for(uint i=0; i<p1.size(); i++){
        Vector2 v(p1[i].x-p2[i].x, p1[i].y-p2[i].y);
        float vit = glm::length(v);
        if(vit>val){
            p1b.push_back(p1[i]);
            p2b.push_back(p2[i]);
        }
    }
    p1=p1b;
    p2=p2b;
}


void DisplayFrame(std::string nameFrame, Mat frame, Points p1, Points p2, Objects objects) {

    Mat f = frame;
    for(uint i=0;i<p1.size();i++){
        cv::arrowedLine(f,p1[i],p2[i],CV_RGB(200,10,10),5);
    }

    auto font = cv::FONT_HERSHEY_SIMPLEX;

    for(auto o:objects){
        cv::Point2f top(o.second.top_left.x,o.second.top_left.y);
        cv::Point2f bot(o.second.bot_right.x,o.second.bot_right.t);
        cv::rectangle(f,top,bot,250);
        cv::putText(f,std::to_string(o.first),cv::Point(o.second.top_left.x,o.second.top_left.y-3), font, 0.5,250,1,cv::LINE_AA);
    }

    cv::imshow(nameFrame,f);
}

void DisplayFramePoint(std::string nameFrame, Mat frame, Points p){
    Mat f = frame;
    for(uint i=0;i<p.size();i++){
        cv::circle(f,p[i],1,CV_RGB(200,10,10),5);
    }

    cv::imshow(nameFrame,f);

}

void MergeRec(Object &o1, Object &o2){

    o1.top_left.x = std::min(o1.top_left.x, o2.top_left.x);
    o1.top_left.y = std::min(o1.top_left.y, o2.top_left.y);
    o1.bot_right.x = std::max(o1.bot_right.x, o2.bot_right.x);
    o1.bot_right.y = std::max(o1.bot_right.y, o2.bot_right.y);

}

bool toMerge(Object &o1, Object &o2){
    float x1 = o1.top_left.x;
    float x2 = o2.top_left.x;
    float y1 = o1.top_left.y;
    float y2 = o2.top_left.y;
    float w1 = o1.bot_right.x - x1;
    float w2 = o2.bot_right.x - x2;
    float h1 = o1.bot_right.y - y1;
    float h2 = o2.bot_right.y - y2;

    if((x2 >= x1 + w1) ||
            (x2 + w2 <= x1) ||
            (y2 >= y1 + h1) ||
            (y2 + h2 <= y1))
        return false;
    else
        return true;

}

Vector2 getSizeObject(Object o){
    return Vector2(o.bot_right.x-o.top_left.x,o.bot_right.y-o.top_left.y);
}

Vector3 getPositionObject(Object o){
    Point3 top_left(o.top_left.x, o.top_left.y,0);
    Point3 bot_right(o.bot_right.x, o.bot_right.y,0);
    return Vector3(0.5)*top_left+Vector3(0.5)*bot_right;
}