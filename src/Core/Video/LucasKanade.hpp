//
// Created by pierre on 02/01/19.
//

#ifndef PROJECT_LUCASKANADE_HPP
#define PROJECT_LUCASKANADE_HPP


#include <Macros.hpp>
#include <map>


class LucasKanade {
public:

    LucasKanade(Mat i, Mat ip, int mode );

    void compute();

    void getRes(Points &p1, Points &p2);


private:


    void computeCV();

    void computeHM();
    void computePixel(int r, int c, double &x, double &y);

    Points interestPoints, resPoints;

    std::map<int, Object> objects;


    Mat I, Iprev;
    int m_mode;
    int hw;
    Mat Vx, Vy, Vxy;
    Mat Ix, Iy, It;

    int max_point =200;
    cv::TermCriteria termCriteria= cv::TermCriteria(cv::TermCriteria::COUNT|cv::TermCriteria::EPS,20,0.03);
    cv::Size subPixWinSize = cv::Size(10,10);
    cv::Size winSize =cv::Size(31,31);


};


#endif //PROJECT_LUCASKANADE_HPP
