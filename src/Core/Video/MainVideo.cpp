//
// Created by pierre on 03/02/19.
//

#include "MainVideo.hpp"
#include "LucasKanade.hpp"
#include "Util.hpp"

MainVideo::MainVideo(){

}

void MainVideo::update(Mat f1, Mat f2){

    frameColor = f1;
    cv::cvtColor(f1, f1, cv::COLOR_RGB2GRAY);
    cv::cvtColor(f2, f2, cv::COLOR_RGB2GRAY);
    frame = f1;

    lk = LucasKanade(f1,f2,methodLK);
    lk.compute();

    lk.getRes(p1,p2);
    filter_low(p1,p2,speedMin);

    tracking.updateTracking(p1,p2);
}

void MainVideo::display(){
    imshow("Frame", frameColor);
}

void MainVideo::displayDebug() {
    DisplayFrame("Frame",frameColor,p1,p2,tracking.getObjects());
}

SceneObjects MainVideo::getSceneObjects() {
    return tracking.getSceneObjects();
}

void MainVideo::setMethodLK(int method) {
    methodLK = method;
}

void MainVideo::setSpeedMin(int speed) {
    speedMin = speed;
}

void MainVideo::cleanUp() {
    tracking.cleanUp();
}