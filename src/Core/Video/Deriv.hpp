//
// Created by pierre on 01/01/19.
//

#ifndef PROJECT_DERIV_HPP
#define PROJECT_DERIV_HPP


#include <Macros.hpp>

class Deriv {
public:

    Deriv(Mat i);
    Deriv(Mat i, Mat ip);

    Mat getIx();
    Mat getIy();
    Mat getIt();

private:

    void computeDeriv();

    Mat I, Iprev;
    Mat Ix, Iy, It;
};


#endif //PROJECT_DERIV_HPP
