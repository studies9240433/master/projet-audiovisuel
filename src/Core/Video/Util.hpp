//
// Created by pierre on 03/02/19.
//

#ifndef PROJECT_UTIL_HPP
#define PROJECT_UTIL_HPP


#include <Macros.hpp>

void filter_low(Points &p1, Points &p2, int val);
void DisplayFrame(std::string nameFrame, Mat frame, Points p1, Points p2, Objects objects);
void MergeRec(Object &o1, Object &o2);
bool toMerge(Object &o1, Object &o2);

Vector2 getSizeObject(Object o);
Vector3 getPositionObject(Object o);

void DisplayFramePoint(std::string nameFrame, Mat frame, Points p);



#endif //PROJECT_UTIL_HPP
