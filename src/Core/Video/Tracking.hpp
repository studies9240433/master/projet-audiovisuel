//
// Created by pierre on 03/02/19.
//

#ifndef PROJECT_TRACKING_HPP
#define PROJECT_TRACKING_HPP


#include <Macros.hpp>
#include <map>

class Tracking {
public:
    Tracking();

    SceneObjects getSceneObjects();
    Objects getObjects();

    void updateTracking(Points p1_, Points p2_);
    void cleanUp();


    double m_dot_product = 0.5;
    int m_distance = 300;
    double m_speed_dif = 5;
    int m_persistence_object = 24;
    int m_minimum_vectors = 5;

private:
    Objects objects;
    Objects objectsFinal;
    SceneObjects sceneObjects;

    int area = 0;

    void search_object();

    void existing_object();
    void new_object();

    void merge_object(Objects &objects);
    void updateSceneObjects();

    Points p1,p2;

};


#endif //PROJECT_TRACKING_HPP
