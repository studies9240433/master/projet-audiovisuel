#include "Gui/MainWindow.hpp"
#include "Test.hpp"

#include <QApplication>
#include <Log/Log.hpp>


int main(int argc, char *argv[]) {

    //Test test;

    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
