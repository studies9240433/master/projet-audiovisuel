//
// Created by argo on 24/12/18.
//

#include <Log/Log.hpp>
#include "Test.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <glm/vec2.hpp>
#include <thread>
#include <zconf.h>

using namespace cv;
using namespace std;

void testVideo() {

    //auto cap = cv::VideoCapture("../../VS/IRIT01.mpg");


    auto cap = cv::VideoCapture(0);

    // Check if camera opened successfully
    if(!cap.isOpened()){
        cout << "Error opening video stream or file" << endl;
        return;
    }

    double fps = 1000000/cap.get(CV_CAP_PROP_FPS);
    Log(logInfo) << "w:"<<fps;

    while(1){

        Mat frame;
        // Capture frame-by-frame
        cap >> frame;

        // If the frame is empty, break immediately
        if (frame.empty())
            break;

        // Display the resulting frame
        imshow( "Frame", frame );

        // Press  ESC on keyboard to exit
        char c=(char)waitKey(1);
        if(c==27)
            break;
        usleep(fps);
    }

    cap.release();


}

Test::Test()
{

    Log(logDebug) << "Test ...";

    //std::thread t(testVideo);
    //t.detach();
    //testVideo();

    Log(logDebug) << "End Test";
}
