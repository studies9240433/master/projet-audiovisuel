# M2 project - Digital audiovisual

Students:
* Charles Beaudonnet
* Pierre Mézières

Paul Sabatier University - 2018/2019

## How to compile

:warning: You'll need **opencv**, **openal** and **sndfile** installed on your system to compile !

To install them on ubuntu, you can do this:

```bash
$ apt-get install libopencv-dev libopenal-dev libsndfile1-dev
```

To compile, we recommend to follow this procedure:

```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
```

You'll find the *executable* in bin folder.
